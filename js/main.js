$(function(){
    ////////////////////////////////////////////////////////////////////////// MODAL
    var boton;
    $('#contacto').on('show.bs.modal', function(e){
        //console.log(e);
        boton = e.relatedTarget;
        var producto = boton.closest('.producto').querySelector(".card-body");
        $('#contacto #data').html(producto.innerHTML);
        $('[data-toggle="tooltip"]').tooltip();

        console.log('El modal contacto se está mostrando');
        // cambiar estilos del boton disparador del evento
        boton.classList.remove("btn-outline-primary");
        boton.classList.add("btn-success");
        boton.disabled = 'disabled';
    })
    $('#contacto').on('shown.bs.modal', function(e){
        console.log('El modal contacto se mostró');
    });
    $('#contacto').on('hide.bs.modal', function(e){
        console.log('El modal contacto se oculta');
        // retornar estilos del boton disparador del evento
        boton.classList.add("btn-outline-primary");
        boton.classList.remove("btn-success");
        boton.disabled = false;
    });
    $('#contacto').on('hidden.bs.modal', function(e){
        console.log('El modal contacto se ocultó');
    });

    ////////////////////////////////////////////////////////////////////////// TOOLTIP
    $('[data-toggle="tooltip"]').tooltip();
    ////////////////////////////////////////////////////////////////////////// POPOVER
    $('[data-toggle="popover"]').popover();
    $('[data-toggle="popover"]').on('shown.bs.popover', function(){
        setTimeout(function(){        
            $('[data-toggle="popover"]').popover('hide');
        },1000);
    })
    ////////////////////////////////////////////////////////////////////////// CAROUSEL
    $('.carousel').carousel({
        interval: 3000
    })
});